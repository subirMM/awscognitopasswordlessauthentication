package com.example.awscognitopasswordlessauthentication

import android.app.Application
import android.util.Log
import com.amazonaws.mobile.client.AWSMobileClient
import com.amazonaws.mobile.client.Callback
import com.amazonaws.mobile.client.UserStateDetails

class MyApp : Application() {

    override fun onCreate() {
        super.onCreate()
        AWSMobileClient.getInstance().initialize(applicationContext, object : Callback<UserStateDetails> {
            override fun onResult(userStateDetails: UserStateDetails?) {
                Log.i("INIT", "onResult: " + userStateDetails?.userState)

            }

            override fun onError(e: Exception?) {
                Log.e("INIT", "Initialization error.", e)

            }
        })
    }
}