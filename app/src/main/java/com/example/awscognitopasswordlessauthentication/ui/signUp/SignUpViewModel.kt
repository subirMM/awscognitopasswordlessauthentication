package com.example.awscognitopasswordlessauthentication.ui.signUp

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUser
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserAttributes
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.SignUpHandler
import com.amazonaws.services.cognitoidentityprovider.model.SignUpResult

class SignUpViewModel(application: Application) : AndroidViewModel(application) {

    val errorLiveData = MutableLiveData<String>()
    val authResponseLiveData = MutableLiveData<SignUpResultStatus>()
    val showLoaderLiveData = MutableLiveData<Boolean>()

    //create CognitoUserAttributes and add user attributes
    val cognitoUserAttributes = CognitoUserAttributes()

    val signUpHandler = object : SignUpHandler {
        override fun onSuccess(user: CognitoUser?, signUpResult: SignUpResult?) {
            //signUp was Successful
            Log.d("TAG", "Sign up Success...isConfirmed" + signUpResult?.isUserConfirmed)

            signUpResult?.let {
                if (!it.isUserConfirmed) {
                    Log.d(
                        "TAG", "Sign up Success...not Confirmed. verification code sent to:"
                                + signUpResult.codeDeliveryDetails.destination
                    )
                    authResponseLiveData.postValue(SignUpResultStatus.VERIFICATION_REQUIRED)
                } else {
                    Log.d("TAG", "Sign up Success...user Confirmed")
                    authResponseLiveData.postValue(SignUpResultStatus.SIGNED_UP_SUCCESSFULLY)
                }
            }
        }

        override fun onFailure(exception: Exception?) {
            Log.d("TAG", "SignUp Failure" + exception?.message)
            errorLiveData.postValue(exception?.message)
        }
    }
}

enum class SignUpResultStatus {
    VERIFICATION_REQUIRED,
    SIGNED_UP_SUCCESSFULLY
}