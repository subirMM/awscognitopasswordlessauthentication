package com.example.awscognitopasswordlessauthentication.ui.signIn

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.awscognitopasswordlessauthentication.repo.SignInRepo
import com.example.awscognitopasswordlessauthentication.util.Constants.AUTH_INITIATED
import com.example.awscognitopasswordlessauthentication.util.Constants.USER_NOT_AUTHENTICATED
import com.example.awscognitopasswordlessauthentication.util.IValidator
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class SignInViewModel(application: Application) : AndroidViewModel(application), IValidator {

    val errorLiveData = MutableLiveData<String>()
    val authResponseLiveData = MutableLiveData<String>()
    val showLoaderLiveData = MutableLiveData<Boolean>()

    fun validateSignInData(email: String) {
        when {
            email.isEmpty() -> onValidationFailure("Please enter username")//in our case email is username
            isNotValid(email) -> onValidationFailure("Please enter a valid username")
            else -> onValidationSuccess(email)
        }
    }

    override fun onValidationFailure(error: String) {
        errorLiveData.postValue(error)
    }

    override fun onValidationSuccess(msg: String) {
        GlobalScope.launch {
            showLoaderLiveData.postValue(true)
            val signInResult = SignInRepo.signIn(msg)
            when (signInResult) {
                AUTH_INITIATED -> authResponseLiveData.postValue(AUTH_INITIATED)
                USER_NOT_AUTHENTICATED -> authResponseLiveData.postValue(USER_NOT_AUTHENTICATED)
                else -> errorLiveData.postValue(signInResult)
            }
            showLoaderLiveData.postValue(false)
        }
    }

    private fun isNotValid(email: String): Boolean {
        return false
    }
}