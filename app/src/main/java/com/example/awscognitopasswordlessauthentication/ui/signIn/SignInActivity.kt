package com.example.awscognitopasswordlessauthentication.ui.signIn

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.awscognitopasswordlessauthentication.R
import com.example.awscognitopasswordlessauthentication.ui.confirmSignin.ConfirmSignInActivity
import com.example.awscognitopasswordlessauthentication.util.Constants.USER_NOT_AUTHENTICATED
import com.example.awscognitopasswordlessauthentication.util.Utils.makeToast
import kotlinx.android.synthetic.main.activity_sign_in.*
import kotlinx.android.synthetic.main.progressbar_center.*

class SignInActivity : AppCompatActivity() {
    private lateinit var viewModel: SignInViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)
        viewModel = ViewModelProviders.of(this).get(SignInViewModel::class.java)

        initView()
    }

    private fun initView() {
        btn_signIn.setOnClickListener {
            viewModel.validateSignInData(et_username.text.toString())
        }
        initListeners()
    }

    private fun initListeners() {
        viewModel.errorLiveData.observe(this, Observer<String> {
            makeToast(it, this)
        })

        viewModel.authResponseLiveData.observe(this, Observer {
            makeToast(it, this)
            if (it == USER_NOT_AUTHENTICATED) {
                //send to otp screen
                moveToConfirmSignInScreen()
            }else{
                //todo move to home screen
            }
        })

        viewModel.showLoaderLiveData.observe(this, Observer {
            if (it == true) progress_bar.visibility = View.VISIBLE
            else progress_bar.visibility = View.GONE
        })
    }

    private fun moveToConfirmSignInScreen() {
        startActivity(Intent(this, ConfirmSignInActivity::class.java))
    }
}