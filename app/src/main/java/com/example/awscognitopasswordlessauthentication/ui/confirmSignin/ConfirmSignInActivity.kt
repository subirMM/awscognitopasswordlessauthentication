package com.example.awscognitopasswordlessauthentication.ui.confirmSignin

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.awscognitopasswordlessauthentication.R
import com.example.awscognitopasswordlessauthentication.util.Utils.makeToast
import kotlinx.android.synthetic.main.activity_confirm_signin.*
import kotlinx.android.synthetic.main.progressbar_center.*

class ConfirmSignInActivity : AppCompatActivity() {

    lateinit var viewModel: ConfirmSignInViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_confirm_signin)
        viewModel = ViewModelProviders.of(this).get(ConfirmSignInViewModel::class.java)
        initView()
    }

    private fun initView() {
        btn_code.setOnClickListener {
            val email = "-EMAIL-" //get email form preference or any data source
            val otp = et_code.text.toString()
            viewModel.verifyOtp(email, otp)
        }

        initListeners()
    }

    private fun initListeners() {
        viewModel.authResponseLiveData.observe(this, Observer {
            makeToast(it, this)
            proceedToNextScreen()
        })
        viewModel.errorLiveData.observe(this, Observer {
            makeToast(it, this)
        })
        viewModel.showLoaderLiveData.observe(this, Observer {
            if (it == true) progress_bar.visibility = View.VISIBLE
            else progress_bar.visibility = View.GONE
        })
    }

    private fun proceedToNextScreen() {
    }
}