package com.example.awscognitopasswordlessauthentication.ui.confirmSignin

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.awscognitopasswordlessauthentication.repo.SignInRepo
import com.example.awscognitopasswordlessauthentication.util.Constants
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class ConfirmSignInViewModel(application: Application) : AndroidViewModel(application) {

    val errorLiveData = MutableLiveData<String>()
    val authResponseLiveData = MutableLiveData<String>()
    val showLoaderLiveData = MutableLiveData<Boolean>()

    fun verifyOtp(email: String, otp: String) {
        GlobalScope.launch {
            showLoaderLiveData.postValue(true)
            val authChallengeResponse = SignInRepo.respondToAuthChallenge(email, otp)
            if (authChallengeResponse == Constants.AUTH_SUCCESS) {
                authResponseLiveData.postValue(Constants.AUTH_SUCCESS)
            } else {
                errorLiveData.postValue(authChallengeResponse)
            }
            showLoaderLiveData.postValue(false)
        }
    }
}