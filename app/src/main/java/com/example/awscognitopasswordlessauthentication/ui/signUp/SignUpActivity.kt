package com.example.awscognitopasswordlessauthentication.ui.signUp

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.awscognitopasswordlessauthentication.R
import com.example.awscognitopasswordlessauthentication.cognito.CognitoSettings
import com.example.awscognitopasswordlessauthentication.ui.signIn.SignInActivity
import com.example.awscognitopasswordlessauthentication.util.RandomPasswordUtil
import kotlinx.android.synthetic.main.activity_sign_up.*
import kotlinx.android.synthetic.main.progressbar_center.*

class SignUpActivity : AppCompatActivity() {

    private val password = RandomPasswordUtil.generateRandomPassword(8).toString()
    lateinit var viewModel: SignUpViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        viewModel = ViewModelProviders.of(this).get(SignUpViewModel::class.java)
        initView()
    }

    private fun initView() {
        btn_signUp.setOnClickListener {
            signUp()
        }

        btn_signIn.setOnClickListener {
            moveToSignInScreen()
        }

        initListeners()
    }

    private fun initListeners() {
        viewModel.authResponseLiveData.observe(this, Observer { })
        viewModel.errorLiveData.observe(this, Observer { })
        viewModel.showLoaderLiveData.observe(this, Observer {
            if (it == true) progress_bar.visibility = View.VISIBLE
            else progress_bar.visibility = View.GONE
        })
    }

    fun signUp() {
        viewModel.cognitoUserAttributes.addAttribute("email", et_email.text.toString())
        viewModel.cognitoUserAttributes.addAttribute("name", et_username.text.toString())

        CognitoSettings.getUserPool(this).signUpInBackground(
            et_email.text.toString(), password,
            viewModel.cognitoUserAttributes, null, viewModel.signUpHandler
        )
    }

    private fun moveToSignInScreen() {
        startActivity(Intent(this, SignInActivity::class.java))
    }
}
