package com.example.awscognitopasswordlessauthentication.util

interface IValidator {

    fun onValidationFailure(error: String)

    fun onValidationSuccess(msg: String)
}