package com.example.awscognitopasswordlessauthentication.util

import android.content.Context
import android.widget.Toast

object Utils {

    fun makeToast(msg: String, context: Context) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
    }
}