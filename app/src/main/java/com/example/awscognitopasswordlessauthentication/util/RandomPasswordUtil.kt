package com.example.awscognitopasswordlessauthentication.util

import java.util.*

object RandomPasswordUtil {

    // This our Password generating method
    fun generateRandomPassword(len: Int): CharArray {
        println("Generating password using random() : ")
        print("Your new password is : ")

        // A strong password has capitalChars, smallChars,
        // numeric value and symbols. So we are using all of
        // them to generate our password
        val capitalChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        val smallChars = "abcdefghijklmnopqrstuvwxyz"
        val numbers = "0123456789"
        val symbols = "!@#$%^&*_=+-/.?<>)"


        val values = capitalChars + smallChars +
                numbers + symbols

        // Using random method
        val randomMethod = Random()

        val password = CharArray(len)

        for (i in 0 until len) {
            // Use of charAt() method : to get character value
            // Use of nextInt() as it is scanning the value as int
            password[i] = values[randomMethod.nextInt(values.length)]

        }
        return password
    }
}