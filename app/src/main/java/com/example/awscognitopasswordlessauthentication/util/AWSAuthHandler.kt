package com.example.awscognitopasswordlessauthentication.util

import android.util.Log
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoDevice
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserSession
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationContinuation
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationDetails
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.ChallengeContinuation
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.MultiFactorAuthenticationContinuation
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.AuthenticationHandler

class AWSAuthHandler : AuthenticationHandler {

    private lateinit var USERNAME: String
    private var continuationCallbackListener: ContinuationCallback? = null
    val password = RandomPasswordUtil.generateRandomPassword(6).toString()

    fun setContinuationCallbackListener(continuationCallback: ContinuationCallback) {
        this.continuationCallbackListener = continuationCallback
    }

    override fun onSuccess(userSession: CognitoUserSession?, newDevice: CognitoDevice?) {
        Log.d(this.toString(), "Login Success...token = " + userSession?.accessToken)
    }

    override fun onFailure(exception: Exception?) {
        Log.d(this.toString(), "Login Failure: " + exception?.message.toString())
    }

    override fun getAuthenticationDetails(
        authenticationContinuation: AuthenticationContinuation?, userId: String?
    ) {
        val authenticationDetails = AuthenticationDetails(
            USERNAME, "", null
        )
        authenticationDetails.setAuthenticationParameter("USERNAME",USERNAME)

        //pass user sign-in credentials to the continuation
        authenticationContinuation?.setAuthenticationDetails(authenticationDetails)

        //allow the sign-in to continue
        authenticationContinuation?.continueTask()
    }

    override fun authenticationChallenge(continuation: ChallengeContinuation?) {
        continuationCallbackListener?.onAuthenticationChallenge(continuation)
    }

    override fun getMFACode(continuation: MultiFactorAuthenticationContinuation?) {
    }

    fun setUsername(username: String) {
        this.USERNAME = username
    }
}

interface ContinuationCallback {
    fun onAuthenticationChallenge(continuation: ChallengeContinuation?)
}