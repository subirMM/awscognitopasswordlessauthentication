package com.example.awscognitopasswordlessauthentication.util

object Constants {

    const val AUTH_INITIATED = "Auth Initiated"
    const val AUTH_SUCCESS = "Auth Successful"
    const val USER_NOT_AUTHENTICATED = "user is not authenticated"
}