package com.example.awscognitopasswordlessauthentication.repo

import android.util.Log
import com.amazonaws.AmazonClientException
import com.amazonaws.auth.AWSCredentials
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationDetails
import com.amazonaws.mobileconnectors.cognitoidentityprovider.exceptions.CognitoNotAuthorizedException
import com.amazonaws.mobileconnectors.cognitoidentityprovider.util.CognitoServiceConstants
import com.amazonaws.services.cognitoidentityprovider.AmazonCognitoIdentityProvider
import com.amazonaws.services.cognitoidentityprovider.AmazonCognitoIdentityProviderClient
import com.amazonaws.services.cognitoidentityprovider.model.InitiateAuthRequest
import com.amazonaws.services.cognitoidentityprovider.model.InitiateAuthResult
import com.amazonaws.services.cognitoidentityprovider.model.RespondToAuthChallengeRequest
import com.example.awscognitopasswordlessauthentication.cognito.CognitoSettings
import com.example.awscognitopasswordlessauthentication.util.Constants.AUTH_INITIATED
import com.example.awscognitopasswordlessauthentication.util.Constants.AUTH_SUCCESS


object SignInRepo {

    lateinit var authResult: InitiateAuthResult
    lateinit var cognitoIdentityProvider: AmazonCognitoIdentityProvider

    /**
     * The user enters their email address on the custom sign-in page
     */
    fun signIn(email: String): String {
        var authResult = ""

        cognitoIdentityProvider = AmazonCognitoIdentityProviderClient(
            getAwsCredentials()
        )
        val authRequest = awsInitiateAuthRequest(email)
        try {
            SignInRepo.authResult = cognitoIdentityProvider.initiateAuth(authRequest)
            SignInRepo.authResult.authenticationResult?.let {
                authResult = AUTH_INITIATED
            } ?: run {
                throw CognitoNotAuthorizedException("user is not authenticated")
            }
        } catch (exception: AmazonClientException) {
            authResult = exception.message ?: "exception"
        }
        return authResult
    }

    private fun getAwsCredentials(): AWSCredentials {
        return object : AWSCredentials {
            override fun getAWSAccessKeyId(): String {
                return CognitoSettings.clientId
            }

            override fun getAWSSecretKey(): String {
                return CognitoSettings.clientSecret
            }
        }
    }

    /**
     * The user enters their email address on the custom sign-in page,
     * which sends it to the Amazon Cognito user pool.
     *
     * @param email used as username
     * @return InitiateAuthRequest
     */
    private fun awsInitiateAuthRequest(email: String): InitiateAuthRequest {
        val authenticationDetails = AuthenticationDetails(email, "", null)
        authenticationDetails.setAuthenticationParameter("USERNAME", email)
        val authRequest = InitiateAuthRequest()
        authRequest.authFlow = CognitoServiceConstants.AUTH_TYPE_INIT_CUSTOM_AUTH
        authRequest.clientId = CognitoSettings.clientId
        authRequest.authParameters = authenticationDetails.authenticationParameters
        return authRequest
    }


    /**
     * The user retrieves the secret login code from their mailbox and enters it on the custom sign-in page,
     * which sends it to the user pool.
     *
     * @param authResult result obtained after successfully creating auth challenge
     * @param email used as username
     * @param otp used as answer of auth challenge
     */
    fun respondToAuthChallenge(email: String, otp: String): String {
        val result: String
        result = try {
            val respondToAuthChallenge = cognitoIdentityProvider
                .respondToAuthChallenge(
                    createChallengeRequest(
                        email,
                        otp
                    )
                )
            val accessToken = respondToAuthChallenge.authenticationResult.accessToken
            saveAccessTokenForFurtherUse(accessToken)
            AUTH_SUCCESS
        } catch (exception: AmazonClientException) {
            Log.d("Exception", exception.message.toString())
            exception.message.toString()
        }
        return result
    }

    /**
     * creating challenge request
     *
     * @param email used as username
     * @param otp used as answer of auth challenge
     * @return RespondToAuthChallengeRequest
     */
    private fun createChallengeRequest(email: String, otp: String): RespondToAuthChallengeRequest {
        val challengeRequest = RespondToAuthChallengeRequest()
        val challengeResponse = HashMap<String, String>()
        challengeResponse["USERNAME"] = email
        challengeResponse["ANSWER"] = otp
        challengeRequest.challengeResponses = challengeResponse
        challengeRequest.challengeName = authResult.challengeName
        challengeRequest.clientId = CognitoSettings.clientId
        challengeRequest.session = authResult.session
        return challengeRequest
    }

    /**
     * this access token will be used as jwt token in future
     */
    private fun saveAccessTokenForFurtherUse(accessToken: String) {
        val accessToken = accessToken
    }
}