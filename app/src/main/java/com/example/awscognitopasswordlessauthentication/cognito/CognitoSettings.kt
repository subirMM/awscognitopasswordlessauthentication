package com.example.awscognitopasswordlessauthentication.cognito

import android.content.Context
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserPool
import com.amazonaws.regions.Regions

object CognitoSettings {
     const val userPoolId: String = "us-east-1_nz7yKt8d8"
     const val clientId: String = "nd5taqi6e3o4vn2olkm7i020q"
     const val clientSecret: String = ""
     val region: Regions = Regions.US_EAST_1

    fun getUserPool(context: Context): CognitoUserPool {
        return CognitoUserPool(context, userPoolId, clientId, clientSecret, region)
    }
}